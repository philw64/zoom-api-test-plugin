<?php
/**
 * Plugin Name: Zoom API Testing
 * Description: Test interface for Zoom API calls
 * Version: 1.0.0
 */

if ( !class_exists( 'ZOOM_API_PLUGIN' ) ) {
    class ZOOM_API_PLUGIN {
        public function __construct()
        {
            if(file_exists(dirname( __FILE__ ) . "/includes/zoom_interface.php")) {
                require_once dirname( __FILE__ ) . "/includes/zoom_interface.php";
                ZOOM_IF::Instance($this); 
            }

            add_action( 'init', array($this, 'zoom_api_post_type') );
            add_filter( 'single_template', array($this, 'zoom_api_template'), 50, 1 );
        }
       
        // Register Custom Post Type
        function zoom_api_post_type() {

            $labels = array(
                    'name'                       => _x( 'ZoomAPIs', 'Post Type General Name', 'text_domain' ),
                    'singular_name'              => _x( 'ZoomAPI', 'Post Type  Singular Name', 'text_domain' ),
                    'menu_name'                  => __( 'Zoom API', 'text_domain' ),
                    'all_items'                  => __( 'All Items', 'text_domain' ),
                    'parent_item'                => __( 'Parent Item', 'text_domain' ),
                    'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
                    'new_item_name'              => __( 'New Item Name', 'text_domain' ),
                    'add_new_item'               => __( 'Add New Item', 'text_domain' ),
                    'edit_item'                  => __( 'Edit Item', 'text_domain' ),
                    'update_item'                => __( 'Update Item', 'text_domain' ),
                    'view_item'                  => __( 'View Item', 'text_domain' ),
                    'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
                    'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
                    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
                    'popular_items'              => __( 'Popular Items', 'text_domain' ),
                    'search_items'               => __( 'Search Items', 'text_domain' ),
                    'not_found'                  => __( 'Not Found', 'text_domain' ),
                    'no_terms'                   => __( 'No items', 'text_domain' ),
                    'items_list'                 => __( 'Items list', 'text_domain' ),
                    'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
            );
            $args = array(
                    'labels'                     => $labels,
                    'hierarchical'               => false,
                    'public'                     => true,
                    'show_ui'                    => true,
                    'show_admin_column'          => true,
                    'show_in_nav_menus'          => true,
                    'show_tagcloud'              => true,
            );
            register_post_type( 'Zoom API', $args );

        }

        function zoom_api_template($template) {
            if ( is_singular( 'zoomapi' ) ) {
                if( !current_user_can('administrator') ) {
                    echo '<h3>Access denied.</h3>';
                    return;
                }
                                     
                $template = sanitize_title(get_field('zoom_api_template'));
                if ( file_exists(  dirname( __FILE__ ) . '/templates/'.$template.'.php' ) ) {
                    $template =  dirname( __FILE__ ) . '/templates/'.$template.'.php' ;
                }
            }
            
            return $template;

        }
        
        public function zoom_api_log($message) {
            if(is_array($message)) {
                $message = json_encode($message);
            }
            $file = fopen(dirname( __FILE__ ) . "/zoom_api.log","a");
            fwrite($file, "\n" . date('Y-m-d h:i:s') . " :: " . $message);
            fclose($file);
        }

    }
}

$zoomAPI= new ZOOM_API_PLUGIN();

?>

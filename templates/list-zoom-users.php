<?php

/*
 * Returns all zoom users attached to a pro-account.
 */

$zoomIf = ZOOM_IF::Instance();

$response = $zoomIf->get_users();
if ($response['success'] === false) {
    echo "Errors:".var_export($response,true);
} else {
    printf("<h2>Response code: %d</h2>",$response['code']);
    foreach ($response['emails'] as $email) {
        echo '<p>    '.$email.'</p>';
    }
}


?>
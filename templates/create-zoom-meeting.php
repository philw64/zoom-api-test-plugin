<?php

/*
 * Creates a 60 minute zoom meeting at the date provided as a query
 * parameter. For example:
 *
 * <yourUrl>/zoomapi/create-zoom-meeting/?date=2020-07-21T20:00:00Z
 */

// Obtain Zoom Pro account email from settings on home page.
$user_email = get_field('user_email',ZOOM_IF::HOME_PAGE_ID);

if (isset($_GET['date'])) {
    $meeting_date = $_GET['date'];
} else {
    echo "<h2>Expected ?date=[date]</h2>";
    return;
}

if (strtotime($meeting_date) === false) {
    echo "<h2>Expected ?date=2020-07-21T20:00:00Z or similar</h2>";
    return;
}

$zoomIf = ZOOM_IF::Instance();

/*
 * Refer to Zoom API for options: https://marketplace.zoom.us/docs/api-reference/zoom-api/meetings/meetingcreate
 */
$request = [
    'topic'    => 'My Test Meeting',
    'start'    => $meeting_date,
    'duration' => 60,
    'password' => '111111',
    'user_id'  => $user_email,
];

$response = $zoomIf->create_meeting($request);
if ($response['success'] === false) {
    echo 'Errors: Code=' . $response['code'] . ' Message=' . $response['message'];
} else {
    foreach ($response as $key => $value) {
      echo  "<h2>$key = $value</h2>";
    }
}

?>
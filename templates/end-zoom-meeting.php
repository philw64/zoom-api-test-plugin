<?php

/*
 * Ends the meeting with the supplied id, if it is already in progress.s
 *
 */

if (isset($_GET['id'])) {
    $meetingId = $_GET['id'];
}
$zoomIf = ZOOM_IF::Instance();


if (!isset($meetingId)) {
    echo '<h1>Expected id=meetingId</h1>';
    return;
}

$response = $zoomIf->set_join_without_host($meetingId,false);
$response = $zoomIf->end_meeting($meetingId);
if ($response['success'] === false) {
    // There was an error before the request was event sent to the api
    echo 'Errors: Code=' . $response['code'] . ' Message=' . $response['message'];
} else {
    printf("<h2>Response code: %d</h2>",$response['code']);
}

?>
<?php
/*
 * Sets the join-without-host flag true for the meeting with the id supplied.
 * This allows new attendees, with the password, to join without needing to
 * be adimtted by the host.
 *
 */
if (isset($_GET['id'])) {
    $meetingId = $_GET['id'];
}

$zoomIf = ZOOM_IF::Instance();
if (!isset($meetingId)) {
    echo '<h1>Expected id=meetingId</h1>';
    return;
}

$response = $zoomIf->set_join_without_host($meetingId,true);
if ($response['success'] === false) {
    echo 'Errors: Code=' . $response['code'] . ' Message=' . $response['message'];
} else {
    printf("<h2>Response code: %d</h2>",$response['code']);
}


?>
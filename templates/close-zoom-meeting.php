<?php
/*
 * Sets the join-without-host flag false for the meeting with the id supplied.
 * This prevents new attendees joining but does not end an existing meeting.
 *
 */

if (isset($_GET['id'])) {
    $meetingId = $_GET['id'];
}

$zoomIf = ZOOM_IF::Instance();
if (!isset($meetingId)) {
    echo '<h1>Expected id=meetingId</h1>';
    return;
}

$response = $zoomIf->set_join_without_host($meetingId,false);
if ($response['success'] === false) {
    echo 'Errors: Code=' . $response['code'] . ' Message=' . $response['message'];
} else {
    printf("<h2>Response code: %d</h2>",$response['code']);
}


?>
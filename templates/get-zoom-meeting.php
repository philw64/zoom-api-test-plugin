<?php

/*
 * Dumps data about the meeting retreived with the supplied meeting id. 
 *
 */

if (isset($_GET['id'])) {
    $meetingId = $_GET['id'];
}

$zoomIf = ZOOM_IF::Instance();

/*
 * Refer to Zoom API for options: https://marketplace.zoom.us/docs/api-reference/zoom-api
 */
if (!isset($meetingId)) {
    echo '<h1>Expected id=meetingId</h1>';
    return;
}
$response = $zoomIf->get_meeting($meetingId);

if ($response['success'] === false) {
    echo 'Errors: Code=' . $response['code'] . ' Message=' . $response['message'];
} else {
    printf("<h2>Get Meeting Details. Response code: %d</h2>",$response['code']);
    if (!empty($response['details'])) {
        echo('<p>');
        foreach ($response['details'] as $key => $value) {
            if (!is_array($value)) {
                printf("%s => %s</br></br>",$key, $value);
            } else {
                printf("%s => %s</br></br>",$key, var_export($value,true));
            }
        }
        echo('</p>');
    }
}


?>
<?php


if (isset($_GET['id'])) {
    $meetingId = $_GET['id'];
} 

$zoomIf = ZOOM_IF::Instance();

if (!isset($meetingId)) {
    echo '<h1>Expected id=meetingId</h1>';
    return;
}

// Delete meeting without sending cancellation notification 
$response = $zoomIf->delete_meeting($meetingId,false);
if ($response['success'] === false) {
    echo 'Errors: Code=' . $response['code'] . ' Message=' . $response['message'];
} else {
    printf("<h2>Response code: %d</h2>",$response['code']);
}   

?>
<?php

/*
 * Zoom only records meeting participants if more than one person attended the meeting.
 * Their assumption is that if there is only one participant, that person must be the
 * host, but this is not necessarily true if the meeting is created with 'join-without-host'
 * enabled, or if it is updated to allow that just before the meeting opens.
 */


if (isset($_GET['id'])) {
    $meetingId = $_GET['id'];
}

$zoomIf = ZOOM_IF::Instance();
    /*
     * Refer to Zoom API for options: https://marketplace.zoom.us/docs/api-reference/zoom-api
     */
    if (!isset($meetingId)) {
        echo '<h1>Expected id=meetingId</h1>';
        return;
    }

$response = $zoomIf->list_meeting_participants($meetingId);
if ($response['success'] === false) {
    echo 'Errors: Code=' . $response['code'] . ' Message=' . $response['message'];
} else {
    printf("<h2>Response code: %d</h2>",$response['code']);
    foreach ($response['participants'] as $participants) {
        $name = $participants['name'];
        $email = $participants['user_email'];
        $joined = $participants['join_time'];
        $left = $participants['leave_time'];
        echo "<p>Participant: $name $email from $joined until $left </p>";
    }
}

?>
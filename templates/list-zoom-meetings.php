<?php

// Obtain the Zoom Pro account email from settings on home page.
$user_email = get_field('user_email',ZOOM_IF::HOME_PAGE_ID);

$zoomIf = ZOOM_IF::Instance();

/*
 * Refer to Zoom API for options: https://marketplace.zoom.us/docs/api-reference/zoom-api
 */
$request = [
    'type'            => ZOOM_IF::MEETING_TYPE_SCHEDULED,
    'page_size'       => 300,
    'next_page_token' => '',
    'user_id'         => $user_email,
];

$count=0;

/*
 * Zoom sends a next_page_token in the response if there are more pages to
 * retreive. This must be presented back in the subsequent requests to receive
 * the next page.
 *
 * This can be tested by setting the page_size to a small value in the request
 * above.
 */
do {
    $response = $zoomIf->list_meetings($request);
    if ($response['success'] === false) {
        echo 'Errors: Code=' . $response['code'] . ' Message=' . $response['message'];
        return;
    } else {
        printf("<h2>Response code: %d</h2>",$response['code']);
        foreach ($response['meetings'] as $meeting) {
            $count += 1;
            echo "<p>$count    ".var_export($meeting,true).'</p>';
        }
        $request['next_page_token'] = $response['next_page_token'];
    }
    // Loop until there are no more pages.
} while($request['next_page_token'] !== '');

?>
# Zoom API Test Plugin

A Word Press Plugin for Experimenting With the Zoom API 

Based around the [ZoomAPIWrapper](https://github.com/skwirrel/ZoomAPIWrapper) from skwirrel, released under the MIT licence, 
this plugin provides a simple testbed for experimenting within a WordPress framework.

To use this, you will need a Zoom Pro or Business account. 

To get started, download the zip to your PC and the follow the Wordpress Add Plugin instructions to add to your WordPress site.

Some additional setup is then required to run the plugin from the WordPress fontend.

You will need an additional plugin: The free (or Pro) version of Advanced Custom Fields. This will be used to 
store the zoom-api-secret and zoom-api-key in your WordPress database. You **really** don't want these in plain text in 
a source file.

I created a blog to explain how to set this up, which you can read at [GoPollinate.com](https://gopollinate.com/managing-zoom-meetings-with-wordpress/). 



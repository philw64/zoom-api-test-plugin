<?php

if ( !class_exists( 'ZOOM_IF' ) ) {
    class ZOOM_IF {
         
        /*
         * Refer to Zoom API for options: https://marketplace.zoom.us/docs/api-reference/zoom-api/
         * 
         */
        const SCHEDULED_MEETING         = 2;
        const NO_REGISTRATION_REQUIRED  = 2;
        const REGISTRATION_ONCE         = 1;
        
        const MEETING_TYPE_LIVE         = 'live';
        const MEETING_TYPE_SCHEDULED    = 'scheduled';
        const MEETING_TYPE_UPCOMING     = 'upcoming';
        
        // Obtain this page id from the Admin Dashboard -> Pages. This is used
        // for the Advanced Custom Fields get_field function to obtain 
        // values like the API key.
        const HOME_PAGE_ID              = '12';
                
        protected $zoom   = null;
        protected $parent = null;
        
        /**
         * This code makes the class a singleton, Calling Instance again will 
         * return the same instance pointer.
         */
        public static function Instance($parent = null)
        {
            static $inst = null;
            if ($inst === null && $parent) {
                $inst = new ZOOM_IF($parent);
            }
            return $inst;
        }
        
        private function __construct($parent)
        {
            $this->parent = $parent;
            if(file_exists(dirname( __FILE__ ) . "/ZoomAPIWrapper/ZoomAPIWrapper.php")) {
                require_once dirname( __FILE__ ) . "/ZoomAPIWrapper/ZoomAPIWrapper.php";
                                
                $apiKey = get_field('zoom_api_key',ZOOM_IF::HOME_PAGE_ID);
                $apiSecret = get_field('zoom_api_secret',ZOOM_IF::HOME_PAGE_ID);

                $this->zoom = new ZoomAPIWrapper( $apiKey, $apiSecret );
            } 
       }
       /*
        * HTTP response codes between 200 and 299 are Success codes
        */
       public function successful_http_response($code) {
           if (((int)$code >= 200) && ((int)$code <= 299)) {
               return true;
           }
           return false;
       }
       
       /** 
        * 
        * Returns the emails of users on the account.
        */
       public function get_users() {
           $status = $this->zoom->doRequest('GET','/users',array('status'=>'active'));
           $response = ['success' => false, 'code' => -1,'message' => 'Error in request'];
           
           if ($status !== false) {
                $response['code']       = $this->zoom->responseCode();
                $response['success']    = $this->successful_http_response($response['code']);
                $response['message']    = isset($status['message']) ? $status['message'] : '';
                if (isset($status['users'])) {                   
                    foreach ($status['users'] as $user) {
                        $response['emails'][] = $user['email'];
                    }
                }
            }
            return $response;

       }
       
       /**
        *  Returns an array of past and future meatings owned by the user.
        * 
        */
       public function list_meetings($params) {
            $query = [  
              "type"             =>   $params['type'],
              "page_size"        =>   $params['page_size'],
              "next_page_token"  =>   $params['next_page_token'],
            ];
            
            $response = ['success' => false, 'code' => -1,'message' => 'Error in request'];
            if (isset($params['user_id'])) {
                $status = $this->zoom->doRequest('GET','/users/{userId}/meetings',$query,['userId' => $params['user_id']]);                
            } else {
                $status = false;
                $response['message'] = 'Missing parameter: \'user_id\'';
            }            
           
           if ($status !== false) {
                $response['code']               = $this->zoom->responseCode();
                $response['success']            = $this->successful_http_response($response['code']);
                $response['message']            = isset($status['message']) ? $status['message'] : '';
                $response['total_records']      = isset($status['total_records']) ? $status['total_records'] : '';
                $response['next_page_token']    = isset($status['next_page_token']) ? $status['next_page_token'] : '';
                if (isset($status['meetings'])) {                   
                    $response['meetings'] = $status['meetings'];
                }
            }

            $this->parent->zoom_api_log('List Meeting Response: \n'.var_export($status,true));

            return $response;

       }
       
       /**
        *  Returns an array of participants in the given meeting
        * 
        */
       public function list_meeting_participants($meetingId,$next_page_token='') {
            $query = [
              "page_size"        =>   300,
              "next_page_token"  =>   $next_page_token,
            ];
            
            /*
             * Options: https://marketplace.zoom.us/docs/api-reference/zoom-api/reports/reportmeetingparticipants
             */
            
            $response = ['success' => false, 'code' => -1,'message' => 'Error in request'];
            $status = $this->zoom->doRequest('GET','/report/meetings/{meetingId}/participants',$query,['meetingId' => $meetingId]);                
           
           if ($status !== false) {
                $response['code']               = $this->zoom->responseCode();
                $response['success']            = $this->successful_http_response($response['code']);
                $response['message']            = isset($status['message']) ? $status['message'] : '';
                $response['total_records']      = isset($status['total_records']) ? $status['total_records'] : '';
                $response['next_page_token']    = isset($status['next_page_token']) ? $status['next_page_token'] : '';
                if (isset($status['participants'])) {                   
                    $response['participants'] = $status['participants'];
                }
            }

            $this->parent->zoom_api_log('List Meeting Participants: \n'.var_export($status,true));

            return $response;

       }      
       /**
        * 
        * @param type $params
        *   Associative array with the following keys:
        *   topic    => title
        *   start    => Date time in format yyyy-MM-ddT16:00:00[Z] 
        *               Uses local time, or GMT if 'Z' is appended.
        *   duration => In minutes
        *   password => password
        *   user_id  => User email
        * 
        * Returns:
        *   response array:
        *     'success'    => boolean
        *     'code'       => integer if function succeeds
        *     'join_url'   =>
        *     'meeting_id' =>  
        *     'error'      => String if function fails. 
        */
       public function create_meeting($params) {

            /*
             * Refer to Zoom API for options: https://marketplace.zoom.us/docs/api-reference/zoom-api/meetings/meetingcreate
             */

            $meeting = json_encode(
            [
              "topic"       =>         $params['topic'],
              "type"        =>         ZOOM_IF::SCHEDULED_MEETING,
              "start_time"  =>         $params['start'],
              "duration"    =>         $params['duration'],
              "schedule_for"=>         "",
              "timezone"    =>         "",
              "password"    =>         $params['password'],
              "agenda"      =>         "None",
              "settings"        =>         [
                "host_video"        =>         true,
                "participant_video" =>         true,
                "cn_meeting"        =>         false,
                "in_meeting"        =>         false,
                "join_before_host"  =>         false,
                "waiting_room"      =>         false,
                "mute_upon_entry"   =>         false,
                "watermark"         =>         false,
                "use_pmi"           =>         false,
                "approval_type"     =>         ZOOM_IF::NO_REGISTRATION_REQUIRED,
                "registration_type" =>         ZOOM_IF::REGISTRATION_ONCE,
                "audio"             =>         "voip",
                "auto_recording"    =>         "none",
                "enforce_login"     =>         false,
                "enforce_login_domains"          =>         "",
                "alternative_hosts"              =>         "",
                "global_dial_in_countries"       =>         [],
                "registrants_email_notification" =>         true,
                ]
              ]);

            $status = $this->zoom->doRequest('POST','/users/{userId}/meetings',array(),['userId' => $params['user_id']],$meeting);
            $response = ['success' => false, 'code' => -1,'message' => 'Error in request'];
           
            if ($status !== false) {
                $response['code']       = $this->zoom->responseCode(); 
                $response['success']    = $this->successful_http_response($response['code']);
                $response['message']    = isset($status['message']) ? $status['message'] : '';
                $response['meeting_id'] = isset($status['id']) ? $status['id'] : '';
                $response['join_url']   = isset($status['join_url']) ? $status['join_url'] : '';
                $response['password']   = isset($status['password']) ? $status['password'] : '';
                $response['uuid']       = isset($status['uuid']) ? $status['uuid'] : '';
            }

            $this->parent->zoom_api_log('Meeting Create Response: \n'.var_export($status,true));
            return $response;
           
       }
       
       /**
        * Sets join-before-host, so customers can join without needing host to be present
        * 
        * @param meeting_id
        * 
        * 
        * Returns:
        *   response array:
        *     'success'    => boolean
        *     'code'       => integer if function succeeds
        *     'error'      => String if function fails. 
        */
        public function set_join_without_host($meeting_id,$value = false ) {

            /*
             * Refer to Zoom API for options: https://marketplace.zoom.us/docs/api-reference/zoom-api/meetings/meetingupdate
             */

            // A simple POST request
            $update = json_encode(
            [      
              "settings"        =>         [
                "host_video"        =>         true,
                "participant_video" =>         true,
                "cn_meeting"        =>         false,
                "in_meeting"        =>         false,
                "join_before_host"  =>         $value,
                "waiting_room"      =>         false,
                "mute_upon_entry"   =>         false,
                "watermark"         =>         false,
                "use_pmi"           =>         false,
                "approval_type"     =>         ZOOM_IF::NO_REGISTRATION_REQUIRED,
                "global_dial_in_countries"       =>         [],
                "registrants_email_notification" =>         true,
                ]
              ]);

            $status = $this->zoom->doRequest('PATCH','/meetings/{meetingId}',['occurrence_id' => 1],['meetingId' => $meeting_id],$update);
            $response = ['success' => false, 'code' => -1,'message' => 'Error in request'];
           
            if ($status !== false) {
                $response['code']    = $this->zoom->responseCode();
                $response['success'] = $this->successful_http_response($response['code']);
                $response['message'] = isset($status['message']) ? $status['message'] : '';
            }
            $this->parent->zoom_api_log('Meeting Update Response: \n'.var_export($status,true));   
            return $response;
        }
        
        /**
         * 
         * @param type $meeting_id
         * 
         * Returns:
         *   response array:
         *     'success'    => boolean
         *     'code'       => integer if function succeeds
         *     'error'      => String if function fails.          
         */
        public function end_meeting($meeting_id) {
            $update = json_encode(
            [
                 "action"=> "end",
            ]);

            $status = $this->zoom->doRequest('PUT','/meetings/{meetingId}/status',array(),['meetingId' => $meeting_id],$update);

            $response = ['success' => false, 'code' => -1,'message' => 'Error in request'];
           
            if ($status !== false) {
                $response['code']    = $this->zoom->responseCode();
                $response['success'] = $this->successful_http_response($response['code']);
                $response['message'] = isset($status['message']) ? $status['message'] : '';
            }
             $this->parent->zoom_api_log('End Meeting Response: \n'.var_export($status,true));   
            return $response;
        }
        
       /**
         * 
         * @param string $meeting_id
         * @param book   $send_email: Send cancellation notification
         * 
         * Returns:
         *   response array:
         *     'success'    => boolean
         *     'code'       => integer if function succeeds
         *     'error'      => String if function fails.          
         */
        public function delete_meeting($meeting_id, $send_email=true) {
            $query = [
                 "schedule_for_reminder"=> $send_email,
            ];

            $status = $this->zoom->doRequest('DELETE','/meetings/{meetingId}',$query,['meetingId' => $meeting_id],array());
 
            $response = ['success' => false, 'code' => -1,'message' => 'Error in request'];
           
            if ($status !== false) {
                $response['code']    = $this->zoom->responseCode();
                $response['success'] = $this->successful_http_response($response['code']);
                $response['message'] = isset($status['message']) ? $status['message'] : '';
            }
            $this->parent->zoom_api_log('Delete Meeting Response: \n'.var_export($status,true));   
            return $response;
        }        
      /**
         * 
         * @param type $meeting_id
         * 
         * Returns:
         *   response array:
         *     'success'    => boolean
         *     'code'       => integer if function succeeds
         *     'error'      => String if function fails.          
         */
        public function get_meeting($meeting_id) {
            
            $status = $this->zoom->doRequest('GET','/meetings/{meetingId}',array(),['meetingId' => $meeting_id]);
 
            $response = ['success' => false, 'code' => -1,'message' => 'Error in request'];
           
            if ($status !== false) {
                $response['code']    = $this->zoom->responseCode();
                $response['success'] = $this->successful_http_response($response['code']);
                $response['message'] = isset($status['message']) ? $status['message'] : '';
                $response['details'] = $status;
            }
            $this->parent->zoom_api_log('Get Meeting Response: \n'.var_export($status,true));   
            return $response;
        }               
    }
}
?>
